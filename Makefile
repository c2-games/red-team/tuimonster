all: tuimonster

tuimonster: main.go challenges.go
	go build -ldflags "-s -w" -v

clean:
	rm -f tuimonster
