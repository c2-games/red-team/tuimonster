package main

import (
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"log"
	"math/rand"
	"strings"

	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
)

func main() {
	var dimX int
	var dimY int
	var renderCount = 0
	monster := getChallenge()

	if err := ui.Init(); err != nil {
		log.Fatalf("failed to initialize termui: %v", err)
	}
	defer ui.Close()

	// get terminal dimensions
	dimX, dimY = ui.TerminalDimensions()
	img := widgets.NewImage(nil)
	img.Border = true
	img.Image = monster.image
	render := func() {
		// Generate a random location for the image
		x := rand.Int() % dimX
		y := rand.Int() % dimY
		img.SetRect(x, y, x+monster.image.Bounds().Dx()+2, y+monster.image.Bounds().Dy()+2)
		if renderCount > 64 {
			img.Title = monster.prompt
		}
		renderCount++
		ui.Render(img)
	}
	render()

	// Start event loop
	uiEvents := ui.PollEvents()

	// Track successful user input
	var userInput = ""
	for {
		e := <-uiEvents
		if e.ID == "<Resize>" {
			// resize the display and don't spam more
			dimX, dimY = ui.TerminalDimensions()
			continue
		}
		spam := true
		for _, s := range monster.solutions {
			tmp := userInput + e.ID
			if tmp == s {
				// solution hit!
				return
			} else if strings.HasPrefix(s, tmp) {
				spam = false
				userInput = tmp
				break
			}
		}
		if spam {
			userInput = ""
			render()
		}
	}
}
