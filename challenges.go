package main

import (
	"encoding/base64"
	"image"
	"log"
	"math/rand"
	"strings"
)

type challenge struct {
	prompt    string
	solutions []string
	image     image.Image
}

type rawChallenge struct {
	prompt    string
	solutions []string
	image     string
}

var challenges = []rawChallenge{
	{
		"type cookie",
		[]string{"cookie"},
		`iVBORw0KGgoAAAANSUhEUgAAACYAAAAICAYAAACVm43oAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TpSItDnYoIhikOtlFRRxrFYpQIdQKrTqYXPoFTRqSFBdHwbXg4Mdi1cHFWVcHV0EQ/ABxdXFSdJES/5cUWsR6cNyPd/ced+8AoVFhmtUTBzTdNtPJhJjNrYqBV/gxghBGEZGZZcxJUgpdx9c9fHy9i/Gs7uf+HCE1bzHAJxLHmWHaxBvEM5u2wXmfOMxKskp8Tjxh0gWJH7muePzGueiywDPDZiY9TxwmFosdrHQwK5ka8TRxVNV0yheyHquctzhrlRpr3ZO/MJjXV5a5TnMYSSxiCRJEKKihjApsxGjVSbGQpv1EF/+Q65fIpZCrDEaOBVShQXb94H/wu1urMDXpJQUTQO+L43yMAYFdoFl3nO9jx2meAP5n4Epv+6sNYPaT9Hpbix4BA9vAxXVbU/aAyx0g8mTIpuxKfppCoQC8n9E35YDBW6B/zeuttY/TByBDXaVugINDYLxI2etd3t3X2du/Z1r9/QChs3K5HJUPPQAAAAZiS0dEAAAAAAAA+UO7fwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+cDHw4MG6QBF2kAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAAAtElEQVQ4y82TwQrCMBBEZyOoIO0P9JL//6pc+gEqlIrIeLAJQ7rGgwrdUzrJTt5mt8BGw741IEkzM0c7AZhauWmM1O84pOIT/lLtC3RSUJJ8BxWHtNKCJmqyY9Z5F+h5ADsAh9rHlsh6BhCgFXDQKnNLPDOSl1qrLj8CeAC45UJaL1YDKehPW0lybrVWC9dZUpg0xrJn0gadj6LJYPckz5VWXld/gmXdA7g6/nsA90/Dv9l4ApuendF755iQAAAAAElFTkSuQmCC`,
	},
	{
		"type goat 🐐",
		[]string{"goat", "kid"},
		`iVBORw0KGgoAAAANSUhEUgAAABAAAAAICAYAAADwdn+XAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH6AUPEQsijTD9AQAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAABZSURBVCjPnVFbCsAgDEu9mkfIOeO5vEX2M2UOnNNAKaXNAwpM4Jrtmo0TSDojS7IkA2h9ieiRbZdSfhmR7Ly0m/JJHgQiItryffSFtHIgGTuCwyfvms0AgAs+Hyd2dV315QAAAABJRU5ErkJggg==`,
	},
	{
		"type ghost 👻",
		[]string{"spooky", "ghost"},
		`iVBORw0KGgoAAAANSUhEUgAAABAAAAAICAYAAADwdn+XAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH6AUQESMYHhvN0AAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAABKSURBVCjPjY9BEgAgCALB6f9fpksHxtLkok2rCFFIkvxNki+Ok+FuSVQXOOu9jq4LKtfGgACwPpA7P5mY5Mz/zqxucJIqfhlzzdrCKRopZpjacQAAAABJRU5ErkJggg==`,
	},
	{
		"a wild slime!",
		[]string{"slime", "suraimu", "fire", "ice", "wind", "rock", "light", "dark", "zap"},
		`iVBORw0KGgoAAAANSUhEUgAAAA8AAAAQCAYAAADJViUEAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH6AUQETQBf/PhhgAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAEjSURBVCjPrZOtcsJAFIW/ZSpWdiVyccG1rnZleIMiK8Eh6SvUBYmEN2hckdQRl3WJrIxs3K2ALH9phun0zOzs3p17zv2bCx3Q1gl/gbZObJJLl0CvSyB6jDoD9PhvaOsk3ko4N9WurRNtnTSItyL3k1Wo/VJEnRK/iw8ARp9HB7/zVD7DRA8AfL1NqcuNCuQuIkDlMyhzAEw8DgI9bZ30ZwkAg4XH7zzvT+fEKnkGO9zb6Yr+LEFbJ6HbZroGoJhEKKUoJlEgKqXOBBrchVeZUwGDBYhIEDPT9dE+pF75rGXOZU6xGgZiyGr0SrUbX03nGNkOocwxZt7qaMz86i90uz9LzrraikPNdbqkLjdKXW6Qjl9+5dbpcn+fzvnWNWxIDX4AAxebvqqDmFgAAAAASUVORK5CYII=`,
	},
	{
		"🥺 bunny 🥺",
		[]string{"rabbit", "bunny"},
		`iVBORw0KGgoAAAANSUhEUgAAAA0AAAAQCAYAAADNo/U5AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH6AUQExonpX25OQAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAACDSURBVCjPrZMxDoMwDEXfj7gn3cpWjgJb2cpJ3SVxK0MCQnwpSmL52bKdwFaWVzy7UgTMLBqIYGoBNTBxQfdA87I277dkkiQApvcHwPJOtssd/5tUnPY0Dg/31xkggpdr8qHmSLUMPuQuOryevRdfC6bW89mTpF8jzoCl9Qpf4pAD+AJyzz0JkaaK2AAAAABJRU5ErkJggg==`,
	},
}

// Get a prompt, solutions and image pairing
func getChallenge() challenge {
	c := challenges[rand.Int()%len(challenges)]

	pngImg, _, err := image.Decode(base64.NewDecoder(base64.StdEncoding, strings.NewReader(c.image)))
	if err != nil {
		log.Fatalf("failed to decode image: %v", err)
	}
	return challenge{c.prompt, c.solutions, pngImg}
}
